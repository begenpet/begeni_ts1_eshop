package cz.cvut.fel.ts1.shop;

import cz.cvut.fel.ts1.shop.Item;

import java.util.ArrayList;

public class ShoppingCart {

    ArrayList<Item> items;

    public ShoppingCart(ArrayList<Item> items) {
        this.items = items;
    }

    public ShoppingCart() {
        items = new ArrayList<Item>();
    }

    public ArrayList<Item> getCartItems() {
        return items;
    }

    public void addItem(Item temp_item) {
        items.add(temp_item);
        System.out.println("Item with ID " + temp_item.getID() + " added to the shopping cart.");
    }

    public void removeItem(int itemID) {
        for (int i = items.size() - 1; i >= 0; i--) {
            Item temp_item = (Item) items.get(i);
            if (temp_item.getID() == itemID) {
                items.remove(i);
                System.out.println("Item with ID " + temp_item.getID() + " removed from the shopping cart.");
            }
        }
    }

    public int getItemsCount() {
        return items.size();
    }

    public int getTotalPrice() {
        int total = 0;
        for (int i = items.size() - 1; i >= 0; i--) {
            Item temp_item = (Item) items.get(i);
            total += temp_item.getPrice();
        }
        return total;
    }
}
