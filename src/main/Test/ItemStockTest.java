import static org.junit.Assert.*;

import cz.cvut.fel.ts1.shop.Item;
import cz.cvut.fel.ts1.shop.StandardItem;
import cz.cvut.fel.ts1.storage.ItemStock;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class ItemStockTest {

    private Item refItem;
    private int initialCount;
    private int increaseAmount;
    private int decreaseAmount;

    public ItemStockTest(Item refItem, int initialCount, int increaseAmount, int decreaseAmount) {
        this.refItem = refItem;
        this.initialCount = initialCount;
        this.increaseAmount = increaseAmount;
        this.decreaseAmount = decreaseAmount;
    }

    @Parameters
    public static Collection<Object[]> data() {
        Item item = new StandardItem(1, "Test Item", 10.0f, "TestCategory", 5);
        return Arrays.asList(new Object[][] {
                { item, 0, 5, 2 }, // Testing with initial count 0
                { item, 10, 3, 5 }, // Testing with non-zero initial count
                { item, 5, 0, 0 } // Testing with zero increase and decrease amounts
        });
    }

    @Test
    public void testConstructor() {
        ItemStock itemStock = new ItemStock(refItem);
        assertEquals(refItem, itemStock.getItem());
        assertEquals(0, itemStock.getCount());
    }

    @Test
    public void testIncreaseItemCount() {
        ItemStock itemStock = new ItemStock(refItem);
        itemStock.IncreaseItemCount(initialCount);
        assertEquals(initialCount, itemStock.getCount());
        itemStock.IncreaseItemCount(increaseAmount);
        assertEquals(initialCount + increaseAmount, itemStock.getCount());
    }

    @Test
    public void testDecreaseItemCount() {
        ItemStock itemStock = new ItemStock(refItem);
        itemStock.IncreaseItemCount(initialCount);
        itemStock.decreaseItemCount(decreaseAmount);
        assertEquals(initialCount - decreaseAmount, itemStock.getCount());
    }
}