import static org.junit.Assert.*;

import cz.cvut.fel.ts1.shop.Order;
import cz.cvut.fel.ts1.shop.ShoppingCart;
import cz.cvut.fel.ts1.shop.StandardItem;
import org.junit.Test;
import java.util.ArrayList;

public class OrderTest {

    @Test
    public void testConstructorWithState() {
        // Arrange
        ShoppingCart cart = new ShoppingCart();
        cart.addItem(new StandardItem(1, "Test Item", 10.0f, "TestCategory", 5));
        String customerName = "John Doe";
        String customerAddress = "123 Main St";
        int state = 1;

        // Act
        Order order = new Order(cart, customerName, customerAddress, state);

        // Assert
        assertEquals(cart.getCartItems(), order.getItems());
        assertEquals(customerName, order.getCustomerName());
        assertEquals(customerAddress, order.getCustomerAddress());
        assertEquals(state, order.getState());
    }

    @Test
    public void testConstructorWithoutState() {
        // Arrange
        ShoppingCart cart = new ShoppingCart();
        cart.addItem(new StandardItem(1, "Test Item", 10.0f, "TestCategory", 5));
        String customerName = "John Doe";
        String customerAddress = "123 Main St";

        // Act
        Order order = new Order(cart, customerName, customerAddress);

        // Assert
        assertEquals(cart.getCartItems(), order.getItems());
        assertEquals(customerName, order.getCustomerName());
        assertEquals(customerAddress, order.getCustomerAddress());
        assertEquals(0, order.getState());
    }
}
