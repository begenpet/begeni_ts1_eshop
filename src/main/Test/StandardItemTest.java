import static org.junit.Assert.*;

import cz.cvut.fel.ts1.shop.StandardItem;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class StandardItemTest {

    private int id;
    private String name;
    private float price;
    private String category;
    private int loyaltyPoints;

    public StandardItemTest(int id, String name, float price, String category, int loyaltyPoints) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.category = category;
        this.loyaltyPoints = loyaltyPoints;
    }

    @Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                {1, "Test Item 1", 10.0f, "TestCategory", 5},
                {2, "Test Item 2", 15.0f, "TestCategory", 10},
                {3, "Test Item 3", 20.0f, "TestCategory", 15}
        });
    }

    @Test
    public void testConstructor() {
        StandardItem item = new StandardItem(id, name, price, category, loyaltyPoints);
        assertEquals(id, item.getID());
        assertEquals(name, item.getName());
        assertEquals(price, item.getPrice(), 0.001); // floating point comparison with delta
        assertEquals(category, item.getCategory());
        assertEquals(loyaltyPoints, item.getLoyaltyPoints());
    }

    @Test
    public void testCopy() {
        StandardItem original = new StandardItem(id, name, price, category, loyaltyPoints);
        StandardItem copy = original.copy();
        assertEquals(original, copy);
    }

    @Test
    public void testEquals() {
        StandardItem item1 = new StandardItem(id, name, price, category, loyaltyPoints);
        StandardItem item2 = new StandardItem(id, name, price, category, loyaltyPoints);
        assertTrue(item1.equals(item2));
    }
}